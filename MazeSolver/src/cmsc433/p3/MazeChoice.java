package cmsc433.p3;

import java.util.Stack;

public class MazeChoice
{
        public MazeChoice parent;
        public Position at;
        public Direction from;
        public Direction to;

        public MazeChoice(Position a, Direction f, Direction t, MazeChoice parent)
        {
            this.parent = parent;
            this.at = a;
            this.from = f;
            this.to = t;
        }
    
    
}
