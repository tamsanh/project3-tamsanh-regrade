package cmsc433.p3;

import java.util.List;

public class GraderSolution
{
    private float time;
    private List<Direction> moves;
    private boolean correct;

    public GraderSolution(float itime, List<Direction> imoves, boolean icorrect)
    {
        time = itime;
        moves = imoves;
        correct = icorrect;
    }

    public float getTime()
    {
        return time;
    }

    public List<Direction> getMoves()
    {
        return moves;
    }

    public boolean isCorrect()
    {
        return correct;
    }
}
