package cmsc433.p3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * An efficient single-threaded depth-first solver (Iterative), written such that it hints
 * part of a possible solution.
 */
public class STMazeSolverDFSIt extends SkippingMazeSolver
{
    public STMazeSolverDFSIt(Maze maze)
    {
        super(maze);
    }

    public List<Direction> solve()
    {
        Move firstMove = new Move(this.maze.getStart(), null, null);
        Move lastMove = this.solve(firstMove);

        if (lastMove == null) { return null; }

        LinkedList<Direction> path = new LinkedList<Direction>();
        Move move = lastMove;

        // First, build the path of all moves where we had to make a decision
        // (choice), by jumping from one move to the one which came before it
        while (move.to != null)
        {
            path.addFirst(move.to);
            move = move.previous;
        }

        // Build the full path from only the positions where we made choices
        return this.pathToFullPath(path);
    }

    /**
     * Builds a path to the end of the maze, starting with the given move.
     * 
     * @param move
     *            The move used to start the search.
     * 
     * @return The last move of the path. In order to obtain the path from the
     *         last move, just follow move.previous, until it's null.
     */
    private Move solve(Move move)
    {
        ArrayList<Move> moves = new ArrayList<Move>(); // The stack for DFS
        moves.add(move);

        Choice nextChoice;

        while (moves.size() > 0)
        {
            // Pop the last move inserted in the stack and explore choices from
            // it
            int last = moves.size() - 1;
            Move m = moves.get(last);
            moves.remove(last);
            try
            {
                if (m.to == null) // If this is the first move
                {
                    // Where is the first location where I have to make a
                    // choice?
                    nextChoice = this.firstChoice(m.from);
                }
                else
                {
                    // Where is the next location where I have to make a choice,
                    // if I go through m.from in the direction of m.to?
                    nextChoice = this.follow(m.from, m.to);
                }
            }
            catch (SolutionFound e)
            {
                return m;
            }

            if (nextChoice.choices.size() > 0)
            {
                // Add all choices from this location to the stack
                for (Direction next : nextChoice.choices)
                {
                    Move nextMove = new Move(nextChoice.at, next, m);
                    moves.add(nextMove);
                }
            }
        }

        return null;
    }
}
