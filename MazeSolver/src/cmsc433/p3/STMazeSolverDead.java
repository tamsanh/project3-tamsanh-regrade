package cmsc433.p3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import cmsc433.p3.SkippingMazeSolver.SolutionFound;

public class STMazeSolverDead extends SkippingMazeSolver
{
    public STMazeSolverDead(Maze maze)
    {
        super(maze);
    }

    /**
     * Find and fill in the dead ends.
     * 
     */
    public List<Direction> solve()
    {
        LinkedList<Choice> choiceStack = new LinkedList<Choice>();

        int width = maze.getWidth();
        int height = maze.getHeight();
        Position start = maze.getStart();
        int left = (int)Math.floor((double)width*((double)1/(double)3));
        int right = (int)Math.floor((double)width*((double)2/(double)3));
        for(int col = left; col < right; col++)
        {
        	for(int row = 0; row < height-1; row++)
        	{
        		if (row==0 &&col==start.col )
        			continue;
        		Position p = new Position(col,row);
        		LinkedList<Direction> moves = maze.getMoves(p);
        		if(moves.size()==1)
        			//Is dead end
        			choiceStack.push((new Choice(p,null,moves)));
        	}
        }
        try
        {
	        for(Choice c:choiceStack)
	        	maze.setColor(followMark(c.at,c.choices.peek(),2).at,0);
        }
        catch(SolutionFound e)
        {
        	
        }
        LinkedList<Choice> choiceStackSol = new LinkedList<Choice>();
        Position st = maze.getStart();
        choiceStackSol.push(new Choice(st,null,maze.getMoves(st)));
        LinkedList<Direction> sol = new LinkedList<Direction>();
        
        return null;
    }
	
}
