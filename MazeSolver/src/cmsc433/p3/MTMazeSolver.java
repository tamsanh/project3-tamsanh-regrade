package cmsc433.p3;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cmsc433.p3.SkippingMazeSolver.SolutionFound;

/**
 * Threads Poll for node at deepest level
 * Thread explores node, keeping track of the current level
 * Explored nodes become new frontier, and are added to its appropriate stack in the MazeTree
 * 	If the stack is not there, create it.
 * Thread repeats until end is found.
 */

public class MTMazeSolver extends SkippingMazeSolver
{
    volatile MazeStack ms = new MazeStack();
    volatile CountDownLatch solFound = new CountDownLatch(1);
    volatile MazeChoice solMz = null;
    final int proc = Runtime.getRuntime().availableProcessors();
    public MTMazeSolver(Maze maze)
    {
        super(maze);
    }
    
    public class MTSolutionFound extends Exception
    {
        public MazeChoice mc;

        public MTSolutionFound(MazeChoice m)
        {
        	mc = m;
        	//Throw up the found solution
        }
    }
    public void follow(MazeChoice mc) throws MTSolutionFound
    {
    	LinkedList<Direction> choices;
        Direction go_to = mc.to, came_from = mc.from;
        Position at = mc.at;
        MazeChoice m=mc;
        at = at.move(go_to);
        do
        {
            if (at.equals(maze.getEnd())) throw new MTSolutionFound(m);
            //if (at.equals(maze.getStart())) throw new MTSolutionFound(m);
            choices = maze.getMoves(at);
            came_from = go_to.reverse();
            choices.remove(came_from);
            if(choices.size()<1)
            	//Tis a dead end!
            	return;
            go_to = choices.getFirst();
           // maze.setColor(at, 2);
            //maze.display.updateDisplay();

            if (choices.size() == 1)
            {
                at = at.move(go_to);
                m = new MazeChoice(at, came_from, go_to, m);
            }
        } while (choices.size() == 1);

        // return new Choice(at,choices);
        ms.add_raw(at, came_from, choices, m);
    }

    class MTRun implements Runnable
    {
	@Override
	public void run(){
		Stack<MazeChoice> sm = new Stack<MazeChoice>();
		MazeChoice m;
		int pop = 10-(8-(proc));
		try {
			while(true)
			{
				sm = ms.pop(pop);
				while(!sm.isEmpty())
				{
					m = sm.pop();
					follow(m);
					if(solFound.getCount()<1)
					{
						return;
					}
				}
			}
		} catch (MTSolutionFound e) {
				solMz = e.mc;
				solFound.countDown();
		}
	}
    }
	
    public List<Direction> solve()
    {
    	Position start = maze.getStart();
    	MazeChoice m = new MazeChoice(start, null, maze.getMoves(start).peekFirst(),null);
    	Choice c = null;
    	
		try {
			c = firstChoice(start);
			ms.add_raw(c.at,c.from,(LinkedList<Direction>)c.choices,null);
	    	//If threads are all waiting, that means it's completely empty, and no one will add.
	    	ExecutorService taskExecutor = Executors.newFixedThreadPool(proc);
	    	for(int i=0; i<proc; i++)
	    		taskExecutor.execute(new MTRun());
	    	//new MTRun().run();
	    	try {
				solFound.await();
			} catch (InterruptedException e) {
			}
	    	taskExecutor.shutdownNow();
		} catch (SolutionFound e2) {
			solFound.countDown();
		}
		try {
			solFound.await();
		} catch (InterruptedException e) {
			return null;
		}
    	m = solMz;
		if(solMz==null)
			return pathToFullPath(maze.getMoves(maze.getStart()));
		else
		{
			LinkedList<Direction> soln = new LinkedList<Direction>();
			while(m!=null)
			{
				soln.addFirst(m.to);
				m = m.parent;
			}
    		return soln;
		}

    }
    
}
