package cmsc433.p3;

import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MazeStack extends Stack<MazeChoice>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public MazeStack()
	{
		super();
	}
	
	public void anotify()
	{
		synchronized(this)
		{
			this.notifyAll();
		}
	}
	
	public void await(long f)
	{
		synchronized(this)
		{
			try {
				this.wait((long) f);
			} catch (InterruptedException e) {
				return;
			}
		}
	}
	
	public synchronized boolean isEmpty()
	{
		return super.isEmpty();
	}
	
	public synchronized MazeChoice pop()
	{
		return super.pop();
	}
	
	public Stack<MazeChoice> pop(int x)
	{
		//Pops a number of choices
		Stack<MazeChoice> ret = new Stack<MazeChoice>();;
		synchronized(this)
		{
			for(int i = 0; i<x; i++)
			{
				try
				{
					ret.add(super.pop());
				}
				catch(EmptyStackException e)
				{
					return ret;
				}
			}
		}
		return ret;
	}
	
	public void add_choice(Choice c, MazeChoice p)
	{
		if(c.isDeadend())
			return;
		Stack<MazeChoice> s = create_mc(c, p);
    	for(MazeChoice mc:s)
    	{
    		synchronized(this)
    		{
    			super.push(mc);
    		}
    	}
	}

	public void add_raw(Position at, Direction f, LinkedList<Direction> dir, MazeChoice p)
	{
		for(Direction d:dir)
    	{
			MazeChoice n = new MazeChoice(at, f, d, p);
    		synchronized(this)
    		{
    			super.push(n);
    		}
    	}
	}
	
    public Stack<MazeChoice> create_mc (Choice c, MazeChoice p)
    {
    	Stack<MazeChoice> ret = new Stack<MazeChoice>();
    	for(Direction t:c.choices)
    	{
    		ret.push((new MazeChoice(c.at,c.from,t,p)));
    	}
    	return ret;
    }
}
