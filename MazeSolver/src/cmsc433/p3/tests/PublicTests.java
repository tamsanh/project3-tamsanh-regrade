package cmsc433.p3.tests;

import java.io.File;
import java.io.IOException;

import cmsc433.p3.GraderSolution;

import junit.framework.TestCase;

public class PublicTests extends TestCase
{

    public static GraderSolution runTest(String inputFile)
    {
        GraderMain m = new GraderMain();

        File file = new File(inputFile);
        if (!file.exists())
        {
            fail("File " + file.getAbsolutePath() + " does not exist.");
        }

        try
        {
            m.read(inputFile);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            fail("ClassNotFoundException while reading maze from: " + inputFile);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            fail("IOException while reading maze from: " + inputFile);
        }

        return m.solve();
    }

    public static void test200x200u()
    {
        GraderSolution gs = runTest("input/200x200u.mz");
        assertTrue(gs.isCorrect());
    }

    public static void test200x200s()
    {
        GraderSolution gs = runTest("input/200x200.mz");
        assertTrue(gs.isCorrect());
    }

    public static void test200x100u()
    {
        GraderSolution gs = runTest("input/200x100u.mz");
        assertTrue(gs.isCorrect());
    }

    public static void test200x100s()
    {
        GraderSolution gs = runTest("input/200x100.mz");
        assertTrue(gs.isCorrect());
    }
}
