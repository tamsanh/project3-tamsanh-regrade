package cmsc433.p3.tests;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import cmsc433.p3.Direction;
import cmsc433.p3.GraderSolution;
import cmsc433.p3.MTMazeSolver;
import cmsc433.p3.Maze;
import cmsc433.p3.MazeSolver;

/**
 * Main entry point for the program. Provides command-line support for
 * generating random mazes and passing maze files to solvers.
 */
public class GraderMain
{
    private Maze maze;
    private boolean solvable;

    /**
     * Method that calls the solvers. To add your solver to the list of solvers
     * that is run, add it to the "solvers" array defined at the top of this
     * method.
     */

    public GraderSolution solve()
    {
        MazeSolver solver = new MTMazeSolver(maze);
        long startTime, endTime;
        float sec;

        System.out.println();
        System.out.println(className(solver.getClass()) + ":");

        startTime = System.currentTimeMillis();
        List<Direction> soln = solver.solve();
        endTime = System.currentTimeMillis();
        sec = (endTime - startTime) / 1000F;

        boolean correct = false;

        if (soln == null)
        {
            if (!solvable)
            {
                System.out.println("Correctly found no solution in " + sec + " seconds.");
                correct = true;
            }
            else
            {
                System.out.println("Incorrectly returned no solution when there is one.");
                correct = false;
            }
        }
        else
        {
            if (maze.checkSolution(soln))
            {
                System.out.println("Correct solution found in " + sec + " seconds.");
                correct = true;
            }
            else
            {
                System.out.println("Incorrect solution found.");
                correct = false;
            }
        }

        return new GraderSolution(sec, soln, correct);
    }

    public static void main(String[] args)
    {
        GraderMain m = new GraderMain();

        if (args.length != 1 && args.length != 2)
        {
            System.out.println("Arguments:");
            System.out.println("  filename");
            System.out.println("    To solve the maze stored in filename.");
            System.exit(-1);
        }
        File file = new File(args[0]);
        if (!file.exists())
        {
            System.out.println("File " + file.getAbsolutePath() + " does not exist.");
            System.exit(-2);
        }

        try
        {
            m.read(args[0]);
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("ClassNotFoundException while reading maze from: " + args[0]);
            e.printStackTrace();
        }
        catch (IOException e)
        {
            System.out.println("IOException while reading maze from: " + args[0]);
            e.printStackTrace();
        }
        m.solve();
    }

    public void read(String filename) throws IOException, ClassNotFoundException
    {
        ObjectInputStream in =
                new ObjectInputStream(new BufferedInputStream(new FileInputStream(filename)));
        maze = (Maze) in.readObject();
        solvable = in.readBoolean();
        in.close();
    }

    private String className(Class<?> cl)
    {
        StringBuffer fullname = new StringBuffer(cl.getName());
        String name = fullname.substring(fullname.lastIndexOf(".") + 1);
        return name;
    }
}
