package cmsc433.p3;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Threads Poll for node at deepest level
 * Thread explores node, keeping track of the current level
 * Explored nodes become new frontier, and are added to its appropriate stack in the MazeTree
 * 	If the stack is not there, create it.
 * Thread repeats until end is found.
 */

public class MTMazeSolver extends SkippingMazeSolver
{
    volatile MazeStack ms = new MazeStack();
    volatile CountDownLatch solFound = new CountDownLatch(1);
    volatile MazeChoice solMz = null;
    volatile Direction solFrom = null;
    public MTMazeSolver(Maze maze)
    {
        super(maze);
    }

    class MTRun implements Runnable
    {

	@Override
	public void run(){
		// TODO Auto-generated method stub

    	MazeChoice m = null;
    	Choice c = null;
    	Direction n = null;
		try {
			while(true)
			{
				if(ms.isEmpty())
					ms.await();
				m = ms.pop();
				if(maze.display!=null)
					c = followMark(m.at,m.to,2);
				else
					c = follow(m.at,m.to);
				while(!c.isDeadend())
				{
					n = c.choices.remove();
					if(!c.isDeadend())
					{
						ms.add_choice(c, m);
						ms.anotify();
					}
					m = new MazeChoice(c.at,n.reverse(),n,m);
					if(maze.display!=null)
						c = followMark(c.at,n,2);
					else
						c = follow(c.at,n);
				}
				if(solFound.getCount()<1)
				{
					return;
				}
			}
		} catch (SolutionFound e) {
			solMz = m;
			solFrom = e.from;
			solFound.countDown();
		}
	}
    }
	
    public List<Direction> solve()
    {
    	Position start = maze.getStart();
    	MazeChoice m = null;
    	Choice c = null;
    	int proc = Runtime.getRuntime().availableProcessors();
		try {
			c = firstChoice(start);
			ms.add_choice(c, null);
	    	//If threads are all waiting, that means it's completely empty, and no one will add.
	    	ExecutorService taskExecutor = Executors.newFixedThreadPool(proc);
	    	for(int i=0; i<proc; i++)
	    		taskExecutor.execute(new MTRun());
	    	//new MTRun().run();
	    	try {
				solFound.await();
			} catch (InterruptedException e) {
			}
	    	taskExecutor.shutdownNow();
		} catch (SolutionFound e2) {
			solFound.countDown();
		}
		try {
			solFound.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			return null;
		}
    	m = solMz;
		if(solMz==null)
			return pathToFullPath(maze.getMoves(maze.getStart()));
		else
		{
			LinkedList<Direction> soln = new LinkedList<Direction>();
			try {
				follow(maze.getEnd(),solFrom);
			} catch (SolutionFound e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while(m!=null && m.from!=null)
			{
				soln.addFirst(m.to);
				try {
					c =follow(m.at,m.from);
				} catch (SolutionFound e1) {
				}
				m = m.parent;
			}
    		return pathToFullPath(soln);
		}

    }
    
}
